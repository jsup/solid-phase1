﻿using System.Xml.Serialization;

namespace Centric.Internship.Phase1
{
    [XmlRoot("Employees")]
    public class Employees
    {
        [XmlElement("Employee")]
        public Employee[] EmployeesCollection { get; set; }
    }
}