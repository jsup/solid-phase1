﻿using System;
using System.Xml.Serialization;

namespace Centric.Internship.Phase1
{
    [Serializable()]
    [XmlRoot("Results")]
    public class Results
    {
        [XmlElement(ElementName = "AverageSalary")]
        public double AverageSalary { get; set; }
    }
}